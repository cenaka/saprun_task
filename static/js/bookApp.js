/**
 * Created by cenaka on 01.04.16.
 */
(function() {
    var bookApp = angular.module("bookApp", []);

    bookApp.factory('books', function($http, auth){
        // booksObject = {};
        // booksObject.updateBooks = function () {
        //          $http.get('/get_books_list').then(function (data) {
        //             booksObject.data = data;
        //         })
        //     }
        var booksObject = {
            data: []
        };
        booksObject.loadData = function (callback) {
            var options = { params: {token: auth.token} };
            $http.get('/get_books_list', options).success(function (data) {
                booksObject.data.length = 0;
                booksObject.data.push.apply(booksObject.data, data);
                callback()
            });
        };
        return booksObject
    });

    bookApp.factory('auth', function () {
        return {
            'login' : "",
            'token' : ""
        }
    });

    bookApp.controller("BookController", function ($scope, $http, books, auth) {
        $scope.books = books.data;
        $scope.add_vote = function (bookid) {
            $http({
                method: "POST",
                url: "add_vote",
                data: {'token': auth.token, 'bookId': bookid}
            }).success(function () {
                books.loadData()
            })
        };
        $scope.delete_vote = function (bookid) {
            $http({
                method: "POST",
                url: "delete_vote",
                data: {'token': auth.token, 'bookId': bookid}
            }).success(function () {
                books.loadData()
            })
        }
    });

    bookApp.controller("ShowLoginController", function ($scope, auth, books, $http) {
        $scope.data = auth;
        $scope.showLoginModal = true;
        $scope.wrongLogin = false;
        $scope.isLogin = function () {
            return $scope.data.login != ""
        };
        $scope.toggleModal = function () {
            $scope.showLoginModal = !$scope.showLoginModal;
        };
        $scope.closeWarning = function () {
            $scope.wrongLogin = false;
        };
        $scope.user = {};

        $scope.auth = function () {
            $http({
                method: 'POST',
                url: 'auth',
                data: $scope.user
            }).then(function (response) {
                auth.login = $scope.user.login;
                auth.token = response.data;
                books.loadData();
                $scope.toggleModal()
            }, function () {
                $scope.wrongLogin = true;
            })


        }
    });


    bookApp.controller('PopupController', function ($scope, $http, books, auth) {
        $scope.showModal = false;
        $scope.showLoginModal = true;
        $scope.isLogin = function () {
            return auth.login != ""
        };
        $scope.toggleModal = function () {
            $scope.showModal = !$scope.showModal;
        };
        $scope.book = {};
        $scope.errorMessage = "";
        $scope.closeWarning = function () {
            $scope.errorMessage = "";
        };
        $scope.submit = function () {

            var formData = new FormData();
            formData.append('cover', this.formFile);
            formData.append('token', auth.token);
            for (var key in $scope.book) {
                formData.append(key, $scope.book[key])
            }

            $http({
                method: 'POST',
                url: 'add_book',
                data: formData,
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function () {
                books.loadData();
                $scope.toggleModal();
            }, function (response) {
                $scope.errorMessage = "Error: " + response.data
            })
        };




    });

    bookApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
    }]);

    bookApp.directive('modal', function () {
        return {
            template: '<div class="modal fade">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
            '<h4 class="modal-title">{{ title }}</h4>' +
            '</div>' +
            '<div class="modal-body" ng-transclude></div>' +
            '</div>' +
            '</div>' +
            '</div>',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title = attrs.title;
                scope.$watch(attrs.visible, function (value) {
                    if (value == true) {
                        $(element).modal('show');
                    }
                    else
                        $(element).modal('hide');
                });

                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });

                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    });
})();

