#!/usr/bin/env python3

import sys

sys.path.append(
    "./libs"
)

import db
from task import app


if __name__ == "__main__":
    if db.db_exist():
        print("DB already exist", file=sys.stderr)
    else:
        print("DB is not exist, should be initialized", file=sys.stderr)
        login, password = db.initialize_db()
        print("Done! Please, use {}|{} to login into app".format(login, password))

    app.run(
        debug=True,
        host=app.config.get("HOST", "localhost"),
        port=app.config.get("PORT", 8888),
        use_reloader=False
    )
