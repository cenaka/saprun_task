from flask import Flask, request, abort
from werkzeug.exceptions import BadRequest

import db
import os

app = Flask(__name__)

UPLOAD_FOLDER = os.path.join(app.static_folder, "img")
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route('/')
def hello_world():
    return app.send_static_file("index.html")


@app.route('/get_books_list', methods=['GET'])
def get_books_list():
    try:
        token = request.values['token']
    except KeyError:
        abort(401)
    user = check_user(token)
    a = db.get_all_books(user)
    return a

def request_data_is_wrong(message):
    response = message
    return response, 400

@app.route('/add_book', methods=['POST'])
def add_book():
    token = None
    try:
        token = request.form['token']
    except KeyError:
        abort(401)
    check_user(token)

    for param in ("title", "author", "isbn", "pages", "year"):
        if param not in request.form:
            return request_data_is_wrong("Some of the fields are missing")

    try:
        title = request.form['title']
        author = request.form['author']
        isbn = request.form['isbn']
        if db.isbn_exist(isbn):
            return request_data_is_wrong("Duplicate ISBN")
        pages = int(request.form['pages'])
        year = int(request.form['year'])
        cover_file = request.files['cover']
        book = db.add_book(title, author, pages, isbn, year)
        cover_file.save(os.path.join(app.config['UPLOAD_FOLDER'], "cover_{}.jpg".format(book.id)))
    except ValueError:
        return request_data_is_wrong("number of pages and year should be number")
    return "OK"


@app.route('/auth', methods=['POST'])
def auth():
    data = request.get_json()
    username = data['login']
    password = data['password']
    token = db.user_autorization(username, password)
    if token:
        return token
    abort(401)


@app.route('/add_vote', methods=['POST'])
def add_vote():
    data = request.get_json()
    token = data['token']
    book_id = data['bookId']
    user = check_user(token)
    db.add_vote(user, book_id)
    return "OK"


@app.route('/delete_vote', methods=['POST'])
def delete_vote():
    data = request.get_json()
    token = data['token']
    book_id = data['bookId']
    user = check_user(token)
    db.delete_vote(user, book_id)
    return "OK"


def check_user(token):
    user = db.get_user(token)
    if not user:
        abort(401)
    return user


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


if __name__ == '__main__':
    app.run(
        debug=True,
        host=app.config.get("HOST", "localhost"),
        port=app.config.get("PORT", 8888))
