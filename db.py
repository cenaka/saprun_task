import os
import random
import string

from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Integer, String, Boolean, and_, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from json import dumps
from sqlalchemy.orm import class_mapper
import hashlib

db_name = 'bookVote.db'
engine = create_engine('sqlite:///{}'.format(db_name))
Base = declarative_base()
Session = sessionmaker(bind=engine)
session = Session()


voting_table = Table('user_votes', Base.metadata,
    Column('book_id', Integer, ForeignKey('books.id')),
    Column('user_id', Integer, ForeignKey('users.id'))
)


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String, unique=True)
    password = Column(String)
    token = Column(String, unique=True)

    voted_for = relationship(
        "Book",
        secondary=voting_table,
        back_populates="users_voted"
    )
    @staticmethod
    def password_hash(password):
        return hashlib.sha256(password.encode("utf-8")).hexdigest()



    def __repr__(self):
        return "<User(user_id='%s', username='%s', password='%s')>" % (self.id, self.username, self.password)

    def __init__(self, username, password):
        self.username = username
        self.password = self.password_hash(password)


class Book(Base):
    __tablename__ = 'books'
    book_score = relationship("BookScore", uselist=False, back_populates="book")
    users_voted = relationship(
        "User",
        secondary=voting_table,
        back_populates="voted_for"
    )
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String)
    author = Column(String)
    pages = Column(Integer)
    isbn = Column(String, unique=True)
    year = Column(Integer)

    def __init__(self, title, author, pages, isbn, year):
        self.author = author
        self.title = title
        self.pages = pages
        self.isbn = isbn
        self.year = year


class BookScore(Base):
    __tablename__ = 'book_score'
    book_id = Column(Integer, ForeignKey("books.id"), primary_key=True)
    book = relationship("Book", back_populates="book_score")
    score = Column(Integer)
    voting_is_open = Column(Boolean)

    def __init__(self, book, voting_is_open=True):
        self.book = book
        self.score = 0
        self.voting_is_open = voting_is_open


def serialize(models):
    res = {}
    for model in models:
        columns = [c.key for c in class_mapper(model.__class__).columns]
        res.update(dict((c, getattr(model, c)) for c in columns))
    return res


def serialize_book(user, row):
    book, book_score = row
    data = serialize(row)
    data['voted'] = user in book.users_voted
    return data


def get_all_books(user):
    return dumps(
        [
            serialize_book(user, row)
            for row in session.query(Book, BookScore).filter(Book.id == BookScore.book_id).all()
        ]
    )


def user_autorization(username, password):
    password = User.password_hash(password)
    users = session.query(User).filter(and_(User.username == username, User.password == password)).all()
    token = rand_string(10)
    if len(users) != 1:
        return None
    for user in users:
        user.token = token
    session.commit()
    return token


def get_user(token):
    users = session.query(User).filter(User.token == token).all()
    if len(users) == 1:
        user, = users
        return user
    return None


def rand_string(N):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(N))


def add_book(title, author, pages, isbn, year):
    book = Book(title, author, pages, isbn, year)
    session.add(book)
    session.commit()

    session.add(BookScore(book))
    session.commit()
    return book


def add_vote(user, book_id):
    books = session.query(Book).filter(Book.id == book_id).all()
    if len(books) == 1:
        book, = books
        user.voted_for.append(book)
        book.book_score.score += 1
        session.commit()


def delete_vote(user, book_id):
    books = session.query(Book).filter(Book.id == book_id).all()
    if len(books) == 1:
        book, = books
        user.voted_for.remove(book)
        book.book_score.score -= 1
        session.commit()

def initialize_db():
    Base.metadata.create_all(engine)
    book1 = add_book("Adaptive Web Design", "Aaron Gustafson", 350, "2-266-11156-6", 2006)
    book2 = add_book("Enchantment: The Art of Changing Hearts, Minds, and Actions", "Guy Kawasaki", 240, "978-1591845836", 2012)
    book3 = add_book("Таящийся у порога", "Лавкрафт, Говард Филлипс", 576, "978-5-389-10601-7", 216)
    main_login = "user1"
    main_password = "password1"
    user1 = User(main_login, main_password)
    user2 = User("user2", "password2")
    session.add_all([user1, user2])
    session.commit()
    add_vote(user1, book1.id)
    add_vote(user2, book1.id)
    add_vote(user2, book3.id)
    book3.book_score.voting_is_open = False
    session.commit()
    return main_login, main_password

def db_exist():
    return os.path.isfile(db_name)

def isbn_exist(isbn):
    return session.query(Book).filter(Book.isbn == isbn).count() > 0